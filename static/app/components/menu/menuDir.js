(function(angular) {
    var app = angular.module("app")
        .directive("menuDir", function() {
            return {
                templateUrl : "app/components/menu/menu.dir.html",
                link: function (scope, element, attrs, controller) {

                    scope.isLoggedIn = function () {
                        return localStorage.getItem('korisnik')
                    }

                    scope.isAdmin = function () {
                        return localStorage.getItem('korisnik') && JSON.parse(localStorage.getItem('korisnik')).uloga == 0
                    }
                }
            };
        });
})(angular);
