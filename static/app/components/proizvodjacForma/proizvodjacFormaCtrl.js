(function(angular) {
    let app = angular
            .module("app")
            .controller("proizvodjacFormaCtrl", ["$http", "$state", "$stateParams", function($http, $state, $stateParams) {
                let that = this;
                
                this.proizvodjac = {
                    "naziv": "",
                    "adresa": ""
                }

                this.izmena = false;

                that.$onInit = function() {
                    if ($stateParams["id"]) {
                        this.izmena = true;
                        this.dobaviProizvodjaca($stateParams["id"])                
                    }
                }

                this.dobaviProizvodjaca = function(id) {
                    $http.get("api/proizvodjac/" + id).then(function(result){
                        that.proizvodjac = result.data;
                    },
                    function(reason) {
                        console.log(reason);
                    });
                }

                this.kreiraj = function() {
                    $http.post("api/proizvodjaci",  that.proizvodjac).then(
                        function(response) {
                            $state.go("proizvodjaci");
                        }, 
                        function(reason) {
                            console.log(reason);
                        });
                }
                this.izmeni = function() {
                    $http.put("api/proizvodjaci/" + this.proizvodjac.id, that.proizvodjac).then(
                        function(response) {
                            $state.go("proizvodjaci");
                        }, 
                        function(reason) {
                            console.log(reason);
                        });
                }
            }]);
})(angular);