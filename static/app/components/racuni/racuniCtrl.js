(function(angular){

    var app = angular.module("app");


    app.controller("racuniCtrl", ["$http" , "$state", function($http, $state) {
        var that = this; 

        this.racuni = []; 
        this.dobaviSveRacune= function() {
            $http.get("api/racuni").then(function(result){
                that.racuni = result.data.racuni;
            },
            function(reason) {
                console.log(reason);
            });
        }
        this.dobaviMojeRacune= function() {
            $http.get("api/moji_racuni").then(function(result){
                that.racuni = result.data.racuni;
            },
            function(reason) {
                console.log(reason);
            });
        }
        this.ukloniRacun = function(id) {
            if (confirm("Da li ste sigurni?")) {
                $http.delete("api/racuni/" + id).then(function(response){
                    that.dobaviSveRacune();                
                },
                function(reason){
                    console.log(reason);
                })
            };
        }
        this.isAdmin = function () {
            return localStorage.getItem('korisnik') && JSON.parse(localStorage.getItem('korisnik')).uloga == 0
        }
        this.isLoggedIn = function () {
            return localStorage.getItem('korisnik')
        }

        if (!this.isLoggedIn()) {
            $state.go('proizvodi')
        }

        if (this.isAdmin()) {
            this.dobaviSveRacune();
        } else {
            this.dobaviMojeRacune();
        }
        
    }]);
})(angular);