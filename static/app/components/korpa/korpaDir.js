(function(angular) {
    var app = angular.module("app")
        .directive("korpaDir", function() {
            return {
                templateUrl : "app/components/korpa/korpa.dir.html",
                // controller: "korpaCtrl",
                scope: {
                  proizvodId: '@',
                  proizvodNaziv: '@',
                  proizvodCena: '@'
                },
                link: function (scope, element, attrs, controller) {

                    scope.proizvod = {
                      kolicina: "",
                      proizvod_id: parseInt(scope.proizvodId),
                      proizvod_naziv: scope.proizvodNaziv,
                      proizvod_cena: scope.proizvodCena
                    }

                    scope.dodajUKorpu = function () {
                      if (!localStorage.getItem("korpa")) {
                        localStorage.setItem("korpa", JSON.stringify([]))
                      }
                      
                      var korpa = JSON.parse(localStorage.getItem("korpa"))

                      for (var stavka of korpa) {
                        if (stavka.proizvod_id == scope.proizvod.proizvod_id) {
                          stavka.kolicina += scope.proizvod.kolicina;
                          localStorage.setItem("korpa", JSON.stringify(korpa))
                          scope.proizvod.kolicina = ""
                          return
                        }
                      }
                      korpa.push(scope.proizvod)
                      localStorage.setItem("korpa", JSON.stringify(korpa))
                      scope.proizvod.kolicina = ""
                    }
                }
            };
        });
})(angular);
