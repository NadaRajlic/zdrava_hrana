(function(angular){

    var app = angular.module("app");


    app.controller("korpaCtrl", ["$http" , "$state", "$stateParams", function($http, $state, $stateParams) {
        var that = this;

        this.korpa = JSON.parse(localStorage.getItem('korpa'))

        this.izracunajUkupno = function() {
          var suma = 0;
          for (var stavka of this.korpa) {
            suma += stavka.kolicina * stavka.proizvod_cena
          }
          return suma
        }

        this.poruci = function() {
          if (!localStorage.getItem('korisnik')) {
            $state.go("login");
            return
          }
          if (this.korpa.length < 1) {
            alert("Nemate proizvoda u korpi")
            return
          }
          $http.post("api/racun", this.korpa).then(
              function(response) {
                  alert("Porudzbina prihvacena")
                  $state.go("proizvodi");
                  this.korpa = [];
                  localStorage.setItem("korpa", JSON.stringify(this.korpa))
              },
              function(reason) {
                  console.log(reason);
              });
        }

        this.ukloni = function(proizvodId) {
          for (var i = 0; i < this.korpa.length; i++) {
            if (this.korpa[i].proizvod_id == proizvodId) {
              this.korpa.splice(i, 1);
              localStorage.setItem("korpa", JSON.stringify(this.korpa))
              return;
            }
          }
        }
    }]);
})(angular);
