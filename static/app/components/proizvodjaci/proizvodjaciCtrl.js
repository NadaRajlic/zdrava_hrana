(function(angular){

    var app = angular.module("app");


    app.controller("proizvodjaciCtrl", ["$http" , "$state", function($http, $state) {
        var that = this; 

        this.proizvodjaci = []; 
        this.dobaviProizvodjace= function() {
            $http.get("api/proizvodjaci").then(function(result){
                that.proizvodjaci = result.data.proizvodjaci;
            },
            function(reason) {
                console.log(reason);
            });
        }
        this.ukloniProizvodjaca = function(id) {
            if (confirm("Da li ste sigurni?")) {
                $http.delete("api/proizvodjaci/" + id).then(function(response){
                    that.dobaviProizvodjace();                
                },
                function(reason){
                    console.log(reason);
                })
            };
        }

        this.isAdmin = function () {
            return localStorage.getItem('korisnik') && JSON.parse(localStorage.getItem('korisnik')).uloga == 0
        }
        
        if (!this.isAdmin()) {
            $state.go('proizvodi')
        }
        
        this.dobaviProizvodjace();
    }]);
})(angular);