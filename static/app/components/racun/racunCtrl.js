(function(angular){

    var app = angular.module("app");


    app.controller("racunCtrl", ["$http" , "$state", "$stateParams", function($http, $state, $stateParams) {
        var that = this;

        this.racun = {};
        this.stavke = [];
        this.dobaviRacun= function() {
            $http.get("api/racun/" + $stateParams['id']).then(function(result){
                that.racun = result.data.racun;
                that.racun.datum_kupovine = new Date(that.racun.datum_kupovine);
                that.stavke = result.data.stavke;
            },
            function(reason) {
                console.log(reason);
            });
        }
    

        this.dobaviRacun();
    }]);
})(angular);
