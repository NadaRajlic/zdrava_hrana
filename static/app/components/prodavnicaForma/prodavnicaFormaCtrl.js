(function(angular) {
    let app = angular
            .module("app")
            .controller("prodavnicaFormaCtrl", ["$http", "$state", "$stateParams", function($http, $state, $stateParams) {
                let that = this;
                
                this.prodavnica = {
                    "naziv": "",
                    "adresa": ""
                }

                this.izmena = false;

                that.$onInit = function() {
                    if ($stateParams["id"]) {
                        this.izmena = true;
                        this.dobaviProdavnicu($stateParams["id"])                
                    }
                }

                this.dobaviProdavnicu = function(id) {
                    $http.get("api/prodavnica/" + id).then(function(result){
                        that.prodavnica = result.data;
                    },
                    function(reason) {
                        console.log(reason);
                    });
                }

                this.kreiraj = function() {
                    $http.post("api/prodavnice",  that.prodavnica).then(
                        function(response) {
                            $state.go("prodavnice");
                        }, 
                        function(reason) {
                            console.log(reason);
                        });
                }
                this.izmeni = function() {
                    $http.put("api/prodavnice/" + this.prodavnica.id, that.prodavnica).then(
                        function(response) {
                            $state.go("prodavnice");
                        }, 
                        function(reason) {
                            console.log(reason);
                        });
                }
            }]);
})(angular);