(function(angular) {
    let app = angular
            .module("app")
            .controller("korisnikFormaCtrl", ["$http", "$state", "$stateParams", function($http, $state, $stateParams) {
                let that = this;
                
                this.korisnik = {
                    "korisnicko_ime": "",
                    "email": "",
                    "ime": "",
                    "prezime": "",
                    "pol": "",
                    "adresa": "",
                    "lozinka": "",
                    "lozinka2": "",
                    "uloga": 1,
                }

                this.izmena = false;

                that.$onInit = function() {
                    if ($stateParams["id"]) {
                        this.izmena = true;
                        this.dobaviKorisnika($stateParams["id"])                
                    }
                }

                this.dobaviKorisnika = function(id) {
                    $http.get("api/korisnik/" + id).then(function(result){
                        that.korisnik = result.data;
                        that.korisnik["lozinka2"] = this.korisnik["lozinka"]
                    },
                    function(reason) {
                        console.log(reason);
                    });
                }

                this.kreiraj = function() {
                    if (this.korisnik.lozinka !== this.korisnik.lozinka2) {
                        alert("Lozinke se ne podudaraju")
                        return
                    }
                    $http.post("api/korisnici",  that.korisnik).then(
                        function(response) {
                            $state.go("proizvodi");
                        }, 
                        function(reason) {
                            if (reason.data == "KORISNIK_POSTOJI") {
                                alert("Korisnicko ime je zauzeto")
                            }
                            console.log(reason);
                        });
                }
                this.izmeni = function() {
                    if (this.korisnik.lozinka !== this.korisnik.lozinka2) {
                        alert("Lozinke se ne podudaraju")
                        return
                    }
                    $http.put("api/korisnici/" + this.korisnik.id, that.korisnik).then(
                        function(response) {
                            $state.go("korisnici");
                        }, 
                        function(reason) {
                            console.log(reason);
                        });
                }
                this.isAdmin = function () {
                    return localStorage.getItem('korisnik') && JSON.parse(localStorage.getItem('korisnik')).uloga == 0
                }
            }]);
})(angular);