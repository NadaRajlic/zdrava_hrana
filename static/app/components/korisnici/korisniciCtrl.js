(function(angular){

    var app = angular.module("app");


    app.controller("korisniciCtrl", ["$http" , "$state", function($http, $state) {
        var that = this; 

        this.korisnici = []; 
        this.tipovi = [];
        this.dobaviKorisnike= function() {
            $http.get("api/korisnici").then(function(result){
                that.korisnici = result.data.korisnici;
            },
            function(reason) {
                console.log(reason);
            });
        }
        this.ukloniKorisnika = function(id) {
            if (confirm("Da li ste sigurni?")) {
                $http.delete("api/korisnici/" + id).then(function(response){
                    that.dobaviKorisnike();                
                },
                function(reason){
                    console.log(reason);
                })
            };
        }
        this.isAdmin = function () {
            return localStorage.getItem('korisnik') && JSON.parse(localStorage.getItem('korisnik')).uloga == 0
        }
        
        if (!this.isAdmin()) {
            $state.go('proizvodi')
        }

        this.dobaviKorisnike();
    }]);
})(angular);