(function(angular){

    var app = angular.module("app");


    app.controller("prodavniceCtrl", ["$http" , "$state", function($http, $state) {
        var that = this; 

        this.prodavnice = []; 
        this.dobaviProdavnice= function() {
            $http.get("api/prodavnice").then(function(result){
                that.prodavnice = result.data.prodavnice;
            },
            function(reason) {
                console.log(reason);
            });
        }
        this.ukloniProdavnicu = function(id) {
            if (confirm("Da li ste sigurni?")) {
                $http.delete("api/prodavnice/" + id).then(function(response){
                    that.dobaviProdavnice();                
                },
                function(reason){
                    console.log(reason);
                })
            };
        }

        this.isAdmin = function () {
            return localStorage.getItem('korisnik') && JSON.parse(localStorage.getItem('korisnik')).uloga == 0
        }
        
        if (!this.isAdmin()) {
            $state.go('proizvodi')
        }
        
        this.dobaviProdavnice();
    }]);
})(angular);