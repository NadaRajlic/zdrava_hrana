(function(angular){

    var app = angular.module("app");


    app.controller("proizvodiCtrl", ["$http" , "$state", "$controller", function($http, $state, $controller) {
        var that = this; 
        var proizvodjaciCtrl = $controller('proizvodjaciCtrl');

        this.proizvodi = []; 
        this.dobaviProizvode= function() {
            $http.get("api/proizvodi").then(function(result){
                that.proizvodi = result.data.proizvodi;
            },
            function(reason) {
                console.log(reason);
            });
        }
        this.ukloniProizvod = function(id) {
            if (confirm("Da li ste sigurni?")) {
                $http.delete("api/proizvodi/" + id).then(function(response){
                    that.dobaviProizvode();                
                },
                function(reason){
                    console.log(reason);
                })
            };
        }
        this.isAdmin = function () {
            return localStorage.getItem('korisnik') && JSON.parse(localStorage.getItem('korisnik')).uloga == 0
        }
        
        this.dobaviProizvode();
    }]);
})(angular);