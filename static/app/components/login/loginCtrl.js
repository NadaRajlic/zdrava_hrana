(function(angular) {
    let app = angular
            .module("app")
            .controller("loginCtrl", ["$http", "$state", function($http, $state) {
                let that = this;
                this.korisnik = {
                    "korisnicko_ime":"",
                    "lozinka":""
                }
                this.login = function() {
                    $http.post("api/login", that.korisnik).then(
                        function(response) {
                            $state.go("proizvodi");
                            localStorage.setItem("korisnik", JSON.stringify(response.data))
                        },
                        function(reason) {
                            console.log(reason);
                            alert('Nepostojece korisnicko ime i/ili lozinka')
                        });
                }
                this.logout = function() {
                    $http.get("api/logout").then(
                        function(response) {
                            $state.go("proizvodi");
                            localStorage.removeItem("korisnik")
                        },
                        function(reason) {
                            console.log(reason);
                        });
                }
            }]);
})(angular);
