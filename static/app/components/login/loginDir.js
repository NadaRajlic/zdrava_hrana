(function(angular) {
    var app = angular.module("app")
        .directive("loginDir", function() {
            return {
                templateUrl : "app/components/login/login.dir.html",
                controller: "loginCtrl",
                link: function (scope, element, attrs, controller) {

                    scope.logout = controller.logout;

                    scope.isLoggedIn = function () {
                        return localStorage.getItem('korisnik')
                    }

                    scope.korpaKolicina = function() {
                        if (!localStorage.getItem('korpa')) {
                            return 0
                        } else {
                            return JSON.parse(localStorage.getItem('korpa')).length
                        }                      
                    }
                }
            };
        });
})(angular);
