(function(angular) {
    let app = angular
            .module("app")
            .controller("proizvodFormaCtrl", ["$http", "$state", "$stateParams", function($http, $state, $stateParams) {
                let that = this;
                
                this.proizvod = {
                    "naziv": "",
                    "zapremina": "",
                    "tezina": "",
                    "sastav": "",
                    "cena": "",
                    "rok_trajanja": "",
                    "proizvodjac_id": "",
                    "prodavnica_id": "",
                }

                this.proizvodjaci = []
                this.prodavnice = []

                this.izmena = false;

                that.$onInit = function() {
                    if ($stateParams["id"]) {
                        this.izmena = true;
                        this.dobaviProizvod($stateParams["id"])                
                    }
                }

                this.dobaviProizvod = function(id) {
                    $http.get("api/proizvod/" + id).then(function(result){
                        that.proizvod = result.data;
                        that.proizvod.proizvodjac_id = that.proizvod.proizvodjac_id.toString()
                        that.proizvod.prodavnica_id = that.proizvod.prodavnica_id.toString()
                    },
                    function(reason) {
                        console.log(reason);
                    });
                }

                this.dobaviProizvodjace= function() {
                    $http.get("api/proizvodjaci").then(function(result){
                        that.proizvodjaci = result.data.proizvodjaci;
                    },
                    function(reason) {
                        console.log(reason);
                    });
                }

                this.dobaviProdavnice= function() {
                    $http.get("api/prodavnice").then(function(result){
                        that.prodavnice = result.data.prodavnice;
                    },
                    function(reason) {
                        console.log(reason);
                    });
                }

                this.kreiraj = function() {
                    if (that.proizvod.zapremina) {
                        that.proizvod.tezina = 0
                    } else {
                        that.proizvod.zapremina = 0
                    }
                    $http.post("api/proizvodi",  that.proizvod).then(
                        function(response) {
                            $state.go("proizvodi");
                        }, 
                        function(reason) {
                            console.log(reason);
                        });
                }
                this.izmeni = function() {
                    if (that.proizvod.zapremina) {
                        that.proizvod.tezina = 0
                    } else {
                        that.proizvod.zapremina = 0
                    }
                    $http.put("api/proizvodi/" + this.proizvod.id, that.proizvod).then(
                        function(response) {
                            $state.go("proizvodi");
                        }, 
                        function(reason) {
                            console.log(reason);
                        });
                }

                this.dobaviProdavnice();
                this.dobaviProizvodjace();
            }]);
})(angular);