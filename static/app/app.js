(function(angular){

    var app = angular.module("app", ["ui.router"]);

    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {

        $stateProvider
        .state({
            name: "korisnici",
            url: "/korisnici",
            templateUrl: "app/components/korisnici/korisnici.tpl.html",
            controller: "korisniciCtrl",
            controllerAs: "korisniciCtrl"
        })
        .state({
            name: "dodajKorisnika",
            url: "/dodaj_korisnika",
            templateUrl: "app/components/korisnikForma/korisnikForma.tpl.html",
            controller: "korisnikFormaCtrl",
            controllerAs: "korisnikFormaCtrl"
        })
        .state({
            name: "izmeniKorisnika",
            url: "/izmeni_korisnika/{id}",
            templateUrl: "app/components/korisnikForma/korisnikForma.tpl.html",
            controller: "korisnikFormaCtrl",
            controllerAs: "korisnikFormaCtrl"
        })

        .state({
            name: "proizvodjaci",
            url: "/proizvodjaci",
            templateUrl: "app/components/proizvodjaci/proizvodjaci.tpl.html",
            controller: "proizvodjaciCtrl",
            controllerAs: "proizvodjaciCtrl"
        })
        .state({
            name: "dodajProizvodjaca",
            url: "/dodaj_proizvodjaca",
            templateUrl: "app/components/proizvodjacForma/proizvodjacForma.tpl.html",
            controller: "proizvodjacFormaCtrl",
            controllerAs: "proizvodjacFormaCtrl"
        })
        .state({
            name: "izmeniProizvodjaca",
            url: "/izmeni_proizvodjaca/{id}",
            templateUrl: "app/components/proizvodjacForma/proizvodjacForma.tpl.html",
            controller: "proizvodjacFormaCtrl",
            controllerAs: "proizvodjacFormaCtrl"
        })

        .state({
            name: "prodavnice",
            url: "/prodavnice",
            templateUrl: "app/components/prodavnice/prodavnice.tpl.html",
            controller: "prodavniceCtrl",
            controllerAs: "prodavniceCtrl"
        })
        .state({
            name: "dodajProdavnicu",
            url: "/dodaj_prodavnicu",
            templateUrl: "app/components/prodavnicaForma/prodavnicaForma.tpl.html",
            controller: "prodavnicaFormaCtrl",
            controllerAs: "prodavnicaFormaCtrl"
        })
        .state({
            name: "izmeniProdavnicu",
            url: "/izmeni_prodavnicu/{id}",
            templateUrl: "app/components/prodavnicaForma/prodavnicaForma.tpl.html",
            controller: "prodavnicaFormaCtrl",
            controllerAs: "prodavnicaFormaCtrl"
        })

        .state({
            name: "proizvodi",
            url: "/proizvodi",
            templateUrl: "app/components/proizvodi/proizvodi.tpl.html",
            controller: "proizvodiCtrl",
            controllerAs: "proizvodiCtrl"
        })
        .state({
            name: "dodajProizvod",
            url: "/dodaj_proizvod",
            templateUrl: "app/components/proizvodForma/proizvodForma.tpl.html",
            controller: "proizvodFormaCtrl",
            controllerAs: "proizvodFormaCtrl"
        })
        .state({
            name: "izmeniProizvod",
            url: "/izmeni_proizvod/{id}",
            templateUrl: "app/components/proizvodForma/proizvodForma.tpl.html",
            controller: "proizvodFormaCtrl",
            controllerAs: "proizvodFormaCtrl"
        })

        .state({
            name: "racuni",
            url: "/racuni",
            templateUrl: "app/components/racuni/racuni.tpl.html",
            controller: "racuniCtrl",
            controllerAs: "racuniCtrl"
        })
        .state({
            name: "racun",
            url: "/racun/{id}",
            templateUrl: "app/components/racun/racun.tpl.html",
            controller: "racunCtrl",
            controllerAs: "racunCtrl"
        })

        .state({
            name: "korpa",
            url: "/korpa",
            templateUrl: "app/components/korpa/korpa.tpl.html",
            controller: "korpaCtrl",
            controllerAs: "korpaCtrl"
        })

        .state({
            name: "login",
            url: "/login",
            templateUrl: "app/components/login/login.tpl.html",
            controller: "loginCtrl",
            controllerAs: "loginCtrl"
        })

        $urlRouterProvider.otherwise("proizvodi")
    }])
})(angular);
