import flask
import datetime
from flask import Flask
from utils.db import mysql
from blueprints.korisnici import korisnici_blueprint
from blueprints.proizvodjaci import proizvodjaci_blueprint
from blueprints.prodavnice import prodavnice_blueprint
from blueprints.proizvodi import proizvodi_blueprint
from blueprints.racuni import racuni_blueprint
from blueprints.login import login_blueprint

app = Flask(__name__, static_url_path="")
app.config['ENV'] = 'development'
app.config['DEBUG'] = True

app.secret_key = 'NadaRajlic'

app.config["MYSQL_DATABASE_USER"] = "root" 
app.config["MYSQL_DATABASE_PASSWORD"] = "root" 
app.config["MYSQL_DATABASE_DB"] = "zdrava_hrana" 
app.config["MYSQL_DATABASE_SOCKET"] = None

mysql.init_app(app) 

app.register_blueprint(korisnici_blueprint, url_prefix="/api")
app.register_blueprint(proizvodjaci_blueprint, url_prefix="/api")
app.register_blueprint(prodavnice_blueprint, url_prefix="/api")
app.register_blueprint(proizvodi_blueprint, url_prefix="/api")
app.register_blueprint(racuni_blueprint, url_prefix="/api")
app.register_blueprint(login_blueprint, url_prefix="/api")

@app.route("/")
@app.route("/index")
def index_page():    
    return app.send_static_file("index.html")

if __name__ == "__main__":
    app.run("0.0.0.0", 5000, threaded=True)
