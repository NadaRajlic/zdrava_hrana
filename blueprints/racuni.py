import flask
from flask.blueprints import Blueprint
from datetime import datetime

from utils.db import mysql;

racuni_blueprint = Blueprint("racuni_blueprint", __name__)

@racuni_blueprint.route("/racuni", methods=["GET"])
def dobavi_racune():
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM racun JOIN korisnik ON korisnik.id = racun.korisnik_id ORDER BY racun.id asc")
    racuni = cursor.fetchall()
    for p in racuni:
        p['datum_kupovine'] = str(p['datum_kupovine'])

    return flask.jsonify({"racuni": racuni})

@racuni_blueprint.route("/moji_racuni", methods=["GET"])
def dobavi_moje_racune():
    username = flask.session.get("korisnik")
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM racun "
        "JOIN korisnik ON racun.korisnik_id = korisnik.id "
        "WHERE korisnicko_ime = %s", (username))
    racuni = cursor.fetchall()
    for p in racuni:
        p['datum_kupovine'] = str(p['datum_kupovine'])

    return flask.jsonify({"racuni": racuni})

@racuni_blueprint.route("/racun/<int:id_racuna>")
def dobavi_racun(id_racuna, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM racun "
        "JOIN korisnik ON racun.korisnik_id = korisnik.id "
        "WHERE racun.id=%s", (id_racuna,))
    racun = cursor.fetchone()
    racun['datum_kupovine'] = str(racun['datum_kupovine'])

    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM proizvod_racun "
        "JOIN proizvod ON proizvod_racun.proizvod_id = proizvod.id "
        "WHERE racun_id=%s", (id_racuna,))
    stavke = cursor.fetchall()

    for stavka in stavke:
        stavka['rok_trajanja'] = str(stavka['rok_trajanja'])

    if racun is not None and stavke is not None:
        return flask.jsonify({
            "racun": racun,
            "stavke": stavke
        })
    else:
        print("DOSLO JE DO GRESKE PRILIKOM DOBAVLJANJA RACUNA")
        return "", 404

@racuni_blueprint.route("/racun", methods=["POST"])
def dodaj_porudzbinu():
    db = mysql.get_db()
    cursor = db.cursor()

    proizvodi = flask.request.json

    username = flask.session.get("korisnik")
    cursor.execute("SELECT * FROM korisnik WHERE korisnicko_ime=%s", (username))
    korisnik = cursor.fetchone()

    ukupno = 0
    for p in proizvodi:
        ukupno += float(p['proizvod_cena']) * int(p['kolicina'])

    racun = {
        'ukupna_cena': ukupno,
        'datum_kupovine': datetime.now(),
        'korisnik_id': korisnik['id']
    }

    cursor.execute("INSERT INTO racun (ukupna_cena, datum_kupovine, korisnik_id) "
        "VALUES (%(ukupna_cena)s, %(datum_kupovine)s, %(korisnik_id)s)", racun)
    db.commit()

    racun_id = cursor.lastrowid

    for p in proizvodi:
        stavka = {
            "kolicina": p['kolicina'],
            "cena": p['proizvod_cena'],
            "proizvod_id": p['proizvod_id'],
            "racun_id": racun_id
        }

        cursor.execute("INSERT INTO proizvod_racun (kolicina, cena, proizvod_id, racun_id) "
        "VALUES (%(kolicina)s, %(cena)s, %(proizvod_id)s, %(racun_id)s)", stavka)
        db.commit()

    return "", 201
