import flask
from flask.blueprints import Blueprint

from utils.db import mysql;

proizvodi_blueprint = Blueprint("proizvodi_blueprint", __name__)

@proizvodi_blueprint.route("/proizvodi", methods=["GET"])
def dobavi_proizvode():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM proizvod "
        "JOIN proizvodjac ON proizvod.proizvodjac_id = proizvodjac.id "
        "JOIN prodavnica ON proizvod.prodavnica_id = prodavnica.id "
        "WHERE proizvod.status=0")
    proizvodi = cursor.fetchall()
    for p in proizvodi:        
        p['rok_trajanja'] = str(p['rok_trajanja'])

    return flask.jsonify({"proizvodi": proizvodi}) 

@proizvodi_blueprint.route("/proizvod/<int:id_proizvoda>")
def dobavi_proizvoda(id_proizvoda, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM proizvod WHERE id=%s and status=0", (id_proizvoda,))
    proizvod = cursor.fetchone() 
    if proizvod is not None:
        proizvod['rok_trajanja'] = str(proizvod['rok_trajanja'])
        return flask.jsonify(proizvod)
    else:
        print("DOSLO JE DO GRESKE PRILIKOM DOBAVLJANJA PROIZVODA")
        return "", 404 

@proizvodi_blueprint.route("/proizvodi", methods=["POST"])
def dodaj_proizvod():
    db = mysql.get_db() 
    cursor = db.cursor() 

    cursor.execute("INSERT INTO proizvod (naziv, zapremina, tezina, sastav, cena, rok_trajanja, proizvodjac_id, prodavnica_id, status) "
        "VALUES (%(naziv)s, %(zapremina)s, %(tezina)s, %(sastav)s, %(cena)s, %(rok_trajanja)s, %(proizvodjac_id)s, %(prodavnica_id)s, 0)", flask.request.json)
  
    db.commit()
    return flask.jsonify(flask.request.json), 201 

@proizvodi_blueprint.route("/proizvodi/<int:id_proizvoda>", methods=["PUT"])
def izmeni_proizvod(id_proizvoda):
   
    
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_proizvoda 
    cursor.execute("UPDATE proizvod SET naziv=%(naziv)s, zapremina=%(zapremina)s, tezina=%(tezina)s, sastav=%(sastav)s, cena=%(cena)s, rok_trajanja=%(rok_trajanja)s, proizvodjac_id=%(proizvodjac_id)s, prodavnica_id=%(prodavnica_id)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@proizvodi_blueprint.route("/proizvodi/<int:id_proizvoda>", methods=["DELETE"])
def ukloni_proizvoda(id_proizvoda):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE proizvod SET status=1 WHERE id=%s", (id_proizvoda))
    db.commit()
    return "", 204 
