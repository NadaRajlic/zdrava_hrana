import flask
from flask.blueprints import Blueprint

from utils.db import mysql;

proizvodjaci_blueprint = Blueprint("proizvodjaci_blueprint", __name__)

@proizvodjaci_blueprint.route("/proizvodjaci", methods=["GET"])
def dobavi_proizvodjace():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM proizvodjac WHERE status=0") 
    proizvodjaci = cursor.fetchall() 
    return flask.jsonify({"proizvodjaci": proizvodjaci}) 

@proizvodjaci_blueprint.route("/proizvodjac/<int:id_proizvodjaca>")
def dobavi_proizvodjaca(id_proizvodjaca, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM proizvodjac WHERE id=%s and status=0", (id_proizvodjaca,))
    proizvodjac = cursor.fetchone() 
    if proizvodjac is not None:
        return flask.jsonify(proizvodjac)
    else:
        print("DOSLO JE DO GRESKE PRILIKOM DOBAVLJANJA PROIZVODJACA")
        return "", 404 

@proizvodjaci_blueprint.route("/proizvodjaci", methods=["POST"])
def dodaj_proizvodjaca():
    db = mysql.get_db() 
    cursor = db.cursor() 

    cursor.execute("INSERT INTO proizvodjac (naziv, adresa, status) "
        "VALUES(%(naziv)s, %(adresa)s, 0)", flask.request.json)
  
    db.commit()
    return flask.jsonify(flask.request.json), 201 

@proizvodjaci_blueprint.route("/proizvodjaci/<int:id_proizvodjaca>", methods=["PUT"])
def izmeni_proizvodjaca(id_proizvodjaca):
   
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_proizvodjaca 
    cursor.execute("UPDATE proizvodjac SET naziv=%(naziv)s, adresa=%(adresa)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@proizvodjaci_blueprint.route("/proizvodjaci/<int:id_proizvodjaca>", methods=["DELETE"])
def ukloni_proizvodjaca(id_proizvodjaca):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE proizvodjac SET status=1 WHERE id=%s", (id_proizvodjaca))
    db.commit()
    return "", 204 
