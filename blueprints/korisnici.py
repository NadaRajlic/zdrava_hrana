import flask
from flask.blueprints import Blueprint

from utils.db import mysql;

korisnici_blueprint = Blueprint("korisnici_blueprint", __name__)

@korisnici_blueprint.route("/korisnici", methods=["GET"])
def dobavi_korisnike():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM korisnik WHERE status=0") 
    korisnici = cursor.fetchall() 
    return flask.jsonify({"korisnici": korisnici}) 

@korisnici_blueprint.route("/korisnik/<int:id_korisnika>")
def dobavi_korisnika(id_korisnika, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM korisnik WHERE id=%s and status=0", (id_korisnika,))
    korisnik = cursor.fetchone() 
    if korisnik is not None:
        return flask.jsonify(korisnik)
    else:
        print("DOSLO JE DO GRESKE PRILIKOM DOBAVLJANJA KORISNIKA")
        return "", 404 

@korisnici_blueprint.route("/korisnici", methods=["POST"])
def dodaj_korisnika():
    db = mysql.get_db() 
    cursor = db.cursor()

    podaciForma = flask.request.json

    cursor.execute("SELECT * FROM korisnik WHERE korisnicko_ime=%s and status=0", (podaciForma['korisnicko_ime'],))
    postojeci = cursor.fetchone()

    if (postojeci is not None):
        return "KORISNIK_POSTOJI", 401

    cursor.execute("INSERT INTO korisnik(korisnicko_ime, email, ime, prezime, pol, adresa, lozinka, uloga, status) "
        "VALUES(%(korisnicko_ime)s, %(email)s, %(ime)s, %(prezime)s, %(pol)s, %(adresa)s, %(lozinka)s, %(uloga)s, 0)", podaciForma)
  
    db.commit()
    return flask.jsonify(flask.request.json), 201 

@korisnici_blueprint.route("/korisnici/<int:id_korisnika>", methods=["PUT"])
def izmeni_korisnika(id_korisnika):
   
    
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_korisnika 
    cursor.execute("UPDATE korisnik SET email=%(email)s, ime=%(ime)s, prezime=%(prezime)s, pol=%(pol)s, adresa=%(adresa)s, lozinka=%(lozinka)s, uloga=%(uloga)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200


@korisnici_blueprint.route("/korisnici/<int:id_korisnika>", methods=["DELETE"])
def ukloni_korisnika(id_korisnika):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE korisnik SET status=1 WHERE id=%s", (id_korisnika))
    db.commit()
    return "", 204 
