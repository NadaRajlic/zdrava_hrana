import flask
from flask.blueprints import Blueprint

from utils.db import mysql;

prodavnice_blueprint = Blueprint("prodavnice_blueprint", __name__)

@prodavnice_blueprint.route("/prodavnice", methods=["GET"])
def dobavi_prodavnice():
    cursor = mysql.get_db().cursor() 
    cursor.execute("SELECT * FROM prodavnica WHERE status=0") 
    prodavnice = cursor.fetchall() 
    return flask.jsonify({"prodavnice": prodavnice}) 

@prodavnice_blueprint.route("/prodavnica/<int:id_prodavnice>")
def dobavi_prodavnicu(id_prodavnice, methods=["GET"]):
    cursor = mysql.get_db().cursor()
    cursor.execute("SELECT * FROM prodavnica WHERE id=%s and status=0", (id_prodavnice,))
    prodavnica = cursor.fetchone() 
    if prodavnica is not None:
        return flask.jsonify(prodavnica)
    else:
        print("DOSLO JE DO GRESKE PRILIKOM DOBAVLJANJA PRODAVNICE")
        return "", 404 

@prodavnice_blueprint.route("/prodavnice", methods=["POST"])
def dodaj_prodavnicu():
    db = mysql.get_db() 
    cursor = db.cursor() 

    cursor.execute("INSERT INTO prodavnica (naziv, adresa, status) "
        "VALUES(%(naziv)s, %(adresa)s, 0)", flask.request.json)
  
    db.commit()
    return flask.jsonify(flask.request.json), 201 

@prodavnice_blueprint.route("/prodavnice/<int:id_prodavnice>", methods=["PUT"])
def izmeni_prodavnicu(id_prodavnice):
   
    db = mysql.get_db()
    cursor = db.cursor()
    data = flask.request.json
    data["id"] = id_prodavnice 
    cursor.execute("UPDATE prodavnica SET naziv=%(naziv)s, adresa=%(adresa)s WHERE id=%(id)s", data)
    db.commit()
    return "", 200

@prodavnice_blueprint.route("/prodavnice/<int:id_prodavnice>", methods=["DELETE"])
def ukloni_prodavnicu(id_prodavnice):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE prodavnica SET status=1 WHERE id=%s", (id_prodavnice))
    db.commit()
    return "", 204 
