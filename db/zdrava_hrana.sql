-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema zdrava_hrana
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `zdrava_hrana` ;

-- -----------------------------------------------------
-- Schema zdrava_hrana
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `zdrava_hrana` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `zdrava_hrana` ;

-- -----------------------------------------------------
-- Table `zdrava_hrana`.`korisnik`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zdrava_hrana`.`korisnik` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `korisnicko_ime` TEXT NOT NULL,
  `email` TEXT NOT NULL,
  `ime` TEXT NOT NULL,
  `prezime` TEXT NOT NULL,
  `pol` ENUM('muški', 'ženski') NOT NULL,
  `adresa` TEXT NOT NULL,
  `lozinka` TEXT NOT NULL,
  `uloga` INT NOT NULL,
  `status` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `zdrava_hrana`.`prodavnica`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zdrava_hrana`.`prodavnica` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `naziv` TEXT NOT NULL,
  `adresa` TEXT NULL,
  `status` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `zdrava_hrana`.`proizvodjac`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zdrava_hrana`.`proizvodjac` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `naziv` TEXT NOT NULL,
  `adresa` TEXT NULL,
  `status` INT NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `zdrava_hrana`.`proizvod`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zdrava_hrana`.`proizvod` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `naziv` TEXT NOT NULL,
  `zapremina` DOUBLE NULL,
  `tezina` DOUBLE NULL,
  `sastav` TEXT NOT NULL,
  `cena` DOUBLE NOT NULL,
  `rok_trajanja` DATE NOT NULL,
  `status` INT NOT NULL,
  `proizvodjac_id` INT(10) NOT NULL,
  `prodavnica_id` INT(10) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_proizvod_prodavnica1_idx` (`prodavnica_id` ASC) VISIBLE,
  INDEX `fk_proizvod_proizvodjac1_idx` (`proizvodjac_id` ASC) VISIBLE,
  CONSTRAINT `fk_proizvod_prodavnica1`
    FOREIGN KEY (`prodavnica_id`)
    REFERENCES `zdrava_hrana`.`prodavnica` (`id`),
  CONSTRAINT `fk_proizvod_proizvodjac1`
    FOREIGN KEY (`proizvodjac_id`)
    REFERENCES `zdrava_hrana`.`proizvodjac` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `zdrava_hrana`.`racun`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zdrava_hrana`.`racun` (
  `id` INT(10) NOT NULL AUTO_INCREMENT,
  `ukupna_cena` DOUBLE NOT NULL,
  `datum_kupovine` DATETIME NOT NULL,
  `korisnik_id` INT(10) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_račun_korisnik_idx` (`korisnik_id` ASC) VISIBLE,
  CONSTRAINT `fk_račun_korisnik`
    FOREIGN KEY (`korisnik_id`)
    REFERENCES `zdrava_hrana`.`korisnik` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `zdrava_hrana`.`proizvod_racun`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `zdrava_hrana`.`proizvod_racun` (
  `kolicina` INT NOT NULL,
  `cena` DOUBLE NOT NULL,
  `proizvod_id` INT(10) NOT NULL,
  `racun_id` INT(10) NOT NULL,
  PRIMARY KEY (`proizvod_id`, `racun_id`),
  INDEX `fk_proizvod_has_racun_racun1_idx` (`racun_id` ASC) VISIBLE,
  INDEX `fk_proizvod_has_racun_proizvod1_idx` (`proizvod_id` ASC) VISIBLE,
  CONSTRAINT `fk_proizvod_has_racun_proizvod1`
    FOREIGN KEY (`proizvod_id`)
    REFERENCES `zdrava_hrana`.`proizvod` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_proizvod_has_racun_racun1`
    FOREIGN KEY (`racun_id`)
    REFERENCES `zdrava_hrana`.`racun` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
