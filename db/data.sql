-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: zdrava_hrana
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `korisnik`
--

DROP TABLE IF EXISTS `korisnik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `korisnik` (
  `id` int NOT NULL AUTO_INCREMENT,
  `korisnicko_ime` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ime` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `prezime` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pol` enum('muški','ženski') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `adresa` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `lozinka` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `uloga` int NOT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `korisnik`
--

LOCK TABLES `korisnik` WRITE;
/*!40000 ALTER TABLE `korisnik` DISABLE KEYS */;
INSERT INTO `korisnik` VALUES (1,'pera','pera@mail.com','perica','peric','muški','Njegoseva 1','pera',0,0),(2,'mira','mira@mira.com','mira','miric','ženski','Novosadskog sajma 3, Novi Sad','mira',1,0),(3,'zika','zika@mail.com','zika','zika','muški','Futoski put 100, Novi Sad','zika',1,0),(4,'tanja','tanja@mail.com','tanja','tanjic','ženski','Resavska 19, Beograd','tanja',1,0),(12,'sanja','sanja@mail.com','sanja','sanjic','ženski','Njegoseva 20, Beograd','sanja',1,0),(13,'admin','admin@mail.com','admin','Adminovic','muški','Cara Dusana 100, Novi Sad','admin',0,0),(14,'pavle','pavle@mail.com','pavle','pavlovic','muški','Takovska 18, Nis','pavle',1,0),(15,'dragan','dragan@mail.com','dragan','draganovic','muški','Homoljska 17, Novi Sad','dragan',1,0);
/*!40000 ALTER TABLE `korisnik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prodavnica`
--

DROP TABLE IF EXISTS `prodavnica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `prodavnica` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `adresa` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prodavnica`
--

LOCK TABLES `prodavnica` WRITE;
/*!40000 ALTER TABLE `prodavnica` DISABLE KEYS */;
INSERT INTO `prodavnica` VALUES (1,'Green Food','Nikole Tesle 5, Novi Sad',0),(2,'Good Food','Mihajla Pupina 10, Novi Sad',0),(3,'Zdrava hrana1','Pap Pavla 29, Novi Sad',0),(4,'Zdrava hrana1','Pap Pavla 29, Novi Sad',1),(5,'Mrvica','Kosovska 19, Beograd',0),(6,'Durum','Njegoseva 10, Beograd',0),(7,'Suncokret','Veselina Maslese 20, Novi Sad',0),(8,'Sunce','Strazilovska 10, Novi Sad',0);
/*!40000 ALTER TABLE `prodavnica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvod`
--

DROP TABLE IF EXISTS `proizvod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proizvod` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `zapremina` double DEFAULT NULL,
  `tezina` double DEFAULT NULL,
  `sastav` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cena` double NOT NULL,
  `rok_trajanja` date NOT NULL,
  `status` int NOT NULL,
  `proizvodjac_id` int NOT NULL,
  `prodavnica_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_proizvod_prodavnica1_idx` (`prodavnica_id`),
  KEY `fk_proizvod_proizvodjac1_idx` (`proizvodjac_id`),
  CONSTRAINT `fk_proizvod_prodavnica1` FOREIGN KEY (`prodavnica_id`) REFERENCES `prodavnica` (`id`),
  CONSTRAINT `fk_proizvod_proizvodjac1` FOREIGN KEY (`proizvodjac_id`) REFERENCES `proizvodjac` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvod`
--

LOCK TABLES `proizvod` WRITE;
/*!40000 ALTER TABLE `proizvod` DISABLE KEYS */;
INSERT INTO `proizvod` VALUES (1,'Hleb1',NULL,0.2,'Heljdino brasno',100,'2020-07-31',0,1,1),(2,'Sojino mleko',0.5,NULL,'Mleko od soje',200,'2020-08-19',0,2,1),(3,'Secer u prahu',0,10,'nepoznat, prirodni',20,'2020-07-31',0,2,1),(4,'Ovsene pahuljice',0,1,'ovas i cokolada',150,'2020-07-30',0,2,3),(5,'Bademovo mleko',1,0,'100% badem',329,'2020-08-31',0,2,3),(6,'Susam',0,0,'susam',60,'2022-09-30',0,1,1),(7,'Lan',0,0,'lan',80,'2021-07-15',0,1,2),(8,'Heljdine plocice',0,0,'Heljdino brasno',49,'2022-12-15',0,5,8),(9,'Keks dijabetski sa malinom',0,0,'Brasno, so, voda, malina',65,'2020-08-20',0,3,5);
/*!40000 ALTER TABLE `proizvod` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvod_racun`
--

DROP TABLE IF EXISTS `proizvod_racun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proizvod_racun` (
  `kolicina` int NOT NULL,
  `cena` double NOT NULL,
  `proizvod_id` int NOT NULL,
  `racun_id` int NOT NULL,
  PRIMARY KEY (`proizvod_id`,`racun_id`),
  KEY `fk_proizvod_has_racun_racun1_idx` (`racun_id`),
  KEY `fk_proizvod_has_racun_proizvod1_idx` (`proizvod_id`),
  CONSTRAINT `fk_proizvod_has_racun_proizvod1` FOREIGN KEY (`proizvod_id`) REFERENCES `proizvod` (`id`),
  CONSTRAINT `fk_proizvod_has_racun_racun1` FOREIGN KEY (`racun_id`) REFERENCES `racun` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvod_racun`
--

LOCK TABLES `proizvod_racun` WRITE;
/*!40000 ALTER TABLE `proizvod_racun` DISABLE KEYS */;
INSERT INTO `proizvod_racun` VALUES (1,100,1,1),(1,100,1,7),(2,100,1,8),(1,100,1,9),(1,100,1,10),(1,100,1,12),(1,100,1,15),(2,100,1,16),(1,100,1,17),(6,100,1,22),(1,100,1,26),(1,100,1,27),(1,200,2,1),(2,400,2,2),(1,200,2,7),(1,200,2,8),(1,200,2,11),(1,200,2,12),(1,200,2,13),(1,200,2,15),(1,200,2,16),(4,200,2,19),(3,200,2,20),(5,200,2,22),(4,200,2,25),(1,200,2,26),(1,200,2,27),(2,10,3,8),(1,20,3,15),(5,20,3,22),(1,20,3,23),(1,20,3,27),(1,150,4,15),(1,150,4,16),(1,150,4,17),(2,150,4,20),(8,150,4,21),(3,150,4,22),(1,150,4,27),(4,329,5,19),(1,329,5,23),(4,329,5,24),(4,329,5,25),(1,329,5,27),(4,60,6,20),(3,60,6,24),(1,60,6,27),(4,80,7,16),(1,80,7,17),(10,80,7,18),(1,80,7,23),(1,80,7,27),(5,49,8,16),(10,49,8,18),(2,49,8,20),(2,49,8,25),(1,49,8,27),(2,65,9,16),(3,65,9,17),(3,65,9,21),(4,65,9,24),(1,65,9,27);
/*!40000 ALTER TABLE `proizvod_racun` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proizvodjac`
--

DROP TABLE IF EXISTS `proizvodjac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `proizvodjac` (
  `id` int NOT NULL AUTO_INCREMENT,
  `naziv` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `adresa` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `status` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proizvodjac`
--

LOCK TABLES `proizvodjac` WRITE;
/*!40000 ALTER TABLE `proizvodjac` DISABLE KEYS */;
INSERT INTO `proizvodjac` VALUES (1,'Tonus','Bulevar Oslobodjenja 12, Novi Sad',0),(2,'Swisslion','Temerinska 32, Novi Sad',0),(3,'Banini','Kisacka 19, Novi Sad',0),(4,'Lucar','Suncana 6, Rakovac',0),(5,'Fani fit','Nikole Tesle 15, Novi Sad',0);
/*!40000 ALTER TABLE `proizvodjac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `racun`
--

DROP TABLE IF EXISTS `racun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `racun` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ukupna_cena` double NOT NULL,
  `datum_kupovine` datetime NOT NULL,
  `korisnik_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_račun_korisnik_idx` (`korisnik_id`),
  CONSTRAINT `fk_račun_korisnik` FOREIGN KEY (`korisnik_id`) REFERENCES `korisnik` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `racun`
--

LOCK TABLES `racun` WRITE;
/*!40000 ALTER TABLE `racun` DISABLE KEYS */;
INSERT INTO `racun` VALUES (1,300,'2020-06-29 00:00:00',1),(2,400,'2020-06-30 00:00:00',1),(7,300,'2020-06-30 18:51:36',1),(8,420,'2020-06-30 20:50:22',2),(9,100,'2020-06-30 21:09:01',2),(10,100,'2020-06-30 21:10:52',2),(11,200,'2020-06-30 21:11:58',1),(12,300,'2020-06-30 21:12:59',1),(13,200,'2020-06-30 21:14:47',2),(15,470,'2020-06-30 22:06:04',3),(16,1245,'2020-07-09 21:04:22',12),(17,525,'2020-07-09 21:04:41',12),(18,1290,'2020-07-09 21:07:57',15),(19,2116,'2020-07-09 21:08:18',15),(20,1238,'2020-07-09 21:08:55',12),(21,1395,'2020-07-09 21:09:11',12),(22,2150,'2020-07-09 21:09:57',13),(23,429,'2020-07-09 21:10:17',13),(24,1756,'2020-07-09 21:11:03',4),(25,2214,'2020-07-09 21:11:20',4),(26,300,'2020-07-09 21:38:52',1),(27,1053,'2020-07-09 22:06:55',13);
/*!40000 ALTER TABLE `racun` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-09 22:08:07
